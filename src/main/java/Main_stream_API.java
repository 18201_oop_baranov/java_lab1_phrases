public class Main_stream_API {
    public static void main(String[] args) {

        Parser parser = new Parser();

        ProgramArgs Prog_args = new ProgramArgs();

        Prog_args = parser.getArgs(args);

        Phrase_calculator calculator = new Phrase_calculator();

        calculator.get_phrases_stream_API(Prog_args);

        return;
    }
}
