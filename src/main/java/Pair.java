public class Pair {
    private String key;
    private Integer value;

    Pair()
    {
        this.key = "";
        this.value = 0;
    }

    Pair(String key, Integer val)
    {
        this.key = key;
        this.value = val;
    }

    public String getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }
}
