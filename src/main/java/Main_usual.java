
public class Main_usual {
    public static void main(String[] args) {
        Parser parser = new Parser();

        ProgramArgs Prog_args = new ProgramArgs();

        Prog_args = parser.getArgs(args);

        Phrase_calculator calculator = new Phrase_calculator();

        calculator.get_phrases(Prog_args);

        return;
    }
}
