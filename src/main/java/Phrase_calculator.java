//package ru.nsu.g.ibaranov.Phrasess;
//import javafx.util.*;
//import org.omg.PortableInterceptor.INACTIVE;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Phrase_calculator {

    String get_phrases(ProgramArgs args)
    {
        String text = "";
        String current = "";

        int len = args.n;
        int min = args.m;
        String filename;
        if (args.in_file_name != null) filename = args.in_file_name;
        else filename = "-";

        int i=0, k=0;

        Map<String, Integer> book = new HashMap<String, Integer>();

        Vector<String> vect = new Vector<String>();

        if (filename.equals("-"))
        {
            Scanner sc = new Scanner(System.in);
            while (sc.hasNext()) {
                text = sc.next();
                if (text.equals("stop")) break;

                k++;
                vect.add(text);
                if (k == len) {
                    for (int j = 0; j < vect.size(); j++) {
                        current += vect.elementAt(j);
                        if (j != vect.size() - 1) current += ' ';
                    }
                    if (book.containsKey(current))
                    {
                        int cur = book.get(current);
                        cur++;
                        book.put(current, cur);
                    }
                    else
                    {
                        book.put(current, 1);
                    }
                    //std::cout << book[current] << std::endl;
                    current = "";
                    vect.remove(vect.firstElement());
                    k--;
                }
            }
            //sc.close();
        }
        else {
            try (FileReader in_file = new FileReader(filename)) {

                Scanner sc = new Scanner(in_file);
                while (sc.hasNext()) {
                    text = sc.next();

                    k++;
                    vect.add(text);
                    if (k == len) {
                        for (int j = 0; j < vect.size(); j++) {
                            current += vect.elementAt(j);
                            if (j != vect.size() - 1) current += ' ';
                        }
                        if (book.containsKey(current)) {
                            int cur = book.get(current);
                            cur++;
                            book.put(current, cur);
                        } else {
                            book.put(current, 1);
                        }
                        //std::cout << book[current] << std::endl;
                        current = "";
                        vect.remove(vect.firstElement());
                        k--;
                    }
                    // sc.close();
                }
            } catch (IOException ioexp) {

            }
        }

       // Vector<Pair<String,Integer>> pair_vect = new Vector<Pair<String,Integer>>();
        Vector<Pair> pair_vect = new Vector<Pair>();

        for (Map.Entry entry: book.entrySet())
        {
            String key = (String)entry.getKey();
            Integer value = (Integer)entry.getValue();
            if (value >= min) pair_vect.add(new Pair(key, value));
        }

        pair_vect.sort((e1, e2) -> e2.getValue().compareTo(e1.getValue()));

        String checkString = "";

        for (Pair p : pair_vect)
        {
            checkString += p.getKey() + " (" + p.getValue() + ")" + '\n';
            System.out.println(p.getKey() + " (" + p.getValue() + ")");
        }

        return checkString;
    }

    int get_phrases_stream_API(ProgramArgs args)
    {
        String text = "";
        String current = "";

        int len = args.n;
        int min = args.m;
        String filename;
        if (args.in_file_name != null) filename = args.in_file_name;
        else filename = "-";

        int i=0, k=0;

        Map<String, Integer> book = new HashMap<String, Integer>();

        Vector<String> vect = new Vector<String>();

        Vector<String> phrases = new Vector<String>();

        if (filename.equals("-"))
        {
            Scanner sc = new Scanner(System.in);
            while (sc.hasNext()) {
                text = sc.next();
                if (text.equals("stop")) break;

                k++;
                vect.add(text);
                if (k == len) {
                    for (int j = 0; j < vect.size(); j++) {
                        current += vect.elementAt(j);
                        if (j != vect.size() - 1) current += ' ';
                    }

                    phrases.add(current);

                    current = "";
                    vect.remove(vect.firstElement());
                    k--;
                }
            }
            //sc.close();
        }
        else {
            try (FileReader in_file = new FileReader(filename)) {

                Scanner sc = new Scanner(in_file);
                while (sc.hasNext()) {
                    text = sc.next();

                    k++;
                    vect.add(text);
                    if (k == len) {
                        for (int j = 0; j < vect.size(); j++) {
                            current += vect.elementAt(j);
                            if (j != vect.size() - 1) current += ' ';
                        }

                        phrases.add(current);

                        current = "";
                        vect.remove(vect.firstElement());
                        k--;
                    }
                    // sc.close();
                }
            } catch (IOException ioexp) {

            }
        }
        phrases.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(((e1, e2) -> (int) (e2.getValue() - e1.getValue())))
                .filter(t -> (t.getValue() >= min))
                .forEach(x -> System.out.printf("%s (%d)\n", x.getKey(), x.getValue()));

        return 0;
    }
}
