//package ru.nsu.g.ibaranov.Phrasess;
import java.io.Console;
import java.io.File;
import java.util.*;

public class Parser {

    ProgramArgs getArgs(String[] args)
    {
        boolean in_file_put = true;

        ProgramArgs programArgs = null;

        int countOfArgs = args.length;

        if (countOfArgs > 7)
        {
            System.out.println("Error in count of arguments");
            return null;
        }

        ProgramArgs Prog_args = new ProgramArgs();

        for (int i=0; i<countOfArgs; ++i)
        {
            if (!(args[i].equals("-n")) && !(args[i].equals("-m")) && !(args[i].equals("-")) && in_file_put)
            {
                try
                {
                    int num = Integer.parseInt(args[i]);
                }
                catch (NumberFormatException npe)
                {
                    String in_file = args[i];
                    Prog_args.in_file_name = in_file;
                    in_file_put = false;
                }
                continue;
            }
            if ((args[i].equals("-")) && (i == countOfArgs-1))
            {
                Prog_args.in_file_name = "-";
                continue;
            }

            if ((args[i].equals("-")) && (i != countOfArgs-1) && (args[i+1].equals("m")))
            {
                if ( i+1 != countOfArgs)
                {Prog_args.m = Integer.parseInt(args[i+2]); continue; }
                else
                {
                    System.out.println("No number for '-m' argument");
                    System.exit(1);
                }
            }
            if ((args[i].equals("-")) && (i != countOfArgs-1) && (args[i+1].equals("n")))
            {
                try {
                    if (i + 1 != countOfArgs + 1) {Prog_args.n = Integer.parseInt(args[i + 2]); continue; }
                    else {
                        System.out.println("No number for '-n' argument");
                        System.exit(1);
                    }
                }
                catch (NumberFormatException nfe)
                {
                    System.out.println("Invalid arguments");
                    throw nfe;
                }
            }
            if ((args[i].equals("-")) && (i != countOfArgs-1) && (!args[i+1].equals("m")) && (!args[i+1].equals("n")))
            {
                System.out.println("Invalid arguments");
                System.exit(1);
            }
            if ((args[i].equals("-m")) && (i != countOfArgs-1))
            {
                try {
                    Prog_args.m = Integer.parseInt(args[i + 1]);
                    continue;
                }
                catch (NumberFormatException nfe)
                {
                    System.out.println("Invalid arguments");
                    throw nfe;
                }
            }
            if ((args[i].equals("-n")) && (i != countOfArgs-1))
            {
                try {
                    Prog_args.n = Integer.parseInt(args[i + 1]);
                    continue;
                }
                catch (NumberFormatException nfe)
                {
                    System.out.println("Invalid arguments");
                    throw  nfe;
                }
            }
        }

        return Prog_args;
    }




}

class ProgramArgs
{
    int n = 2;
    int m = 2;
    String in_file_name = null;

    ProgramArgs()
    {
    }


    ProgramArgs(String in, String n, String m)
    {
        this.in_file_name = in;

        this.n = Integer.parseInt(n);
        this.m = Integer.parseInt(m);
    }

    public int getN() {
        return n;
    }

    public int getM() {
        return m;
    }

    public String getIn_file_name() {
        return in_file_name;
    }
}


