import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Phrase_calculatorTest {

    @Test
    void get_phrases() {
        assertEquals("1",  "1");
        Phrase_calculator calculator = new Phrase_calculator();

        ProgramArgs Prog_args = new ProgramArgs("in.txt", "3", "4");

        String check = calculator.get_phrases(Prog_args);

        assertEquals("yellow submarine yellow (8)\n" +
                "submarine yellow submarine (8)\n" +
                "in a yellow (4)\n" +
                "a yellow submarine (4)\n" +
                "we all live (4)\n" +
                "all live in (4)\n" +
                "live in a (4)\n", check);
    }

}