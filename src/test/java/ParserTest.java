import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    void getArgs() {
        Parser parser = new Parser();

        String[] args = {"-m", "4", "-n", "3", "in.txt"};

        ProgramArgs programArgs = new ProgramArgs();

        programArgs = parser.getArgs(args);

        assertEquals("in.txt", programArgs.getIn_file_name());

        Integer m = programArgs.getM();
        String strm = m.toString();
        assertEquals( "4", strm);

        Integer n = programArgs.getN();
        String strn = n.toString();
        assertEquals( "3", strn);
    }
}